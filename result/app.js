"use strict";
console.log(" ******************************* app lancé ******************************* ");
const express = require('express');
const app = express();
app.set('view engine', 'ejs');
app.set('views', process.cwd() + '/views');
app.use(express.urlencoded({ extended: true }));
app.listen(8080);
