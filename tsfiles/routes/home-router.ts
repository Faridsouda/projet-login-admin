const homeRouter = require('express').Router()
const _homeController = require('../controllers/home-controller')
const _adminController = require('../controllers/admin-controller')
const multer = require('multer');
const upload = multer({dest: 'storage'});

homeRouter.get('',(req:Request,res:Response) => res.redirect('/home'))
homeRouter.get('/home',_homeController.index ) // j'ai fait une erreur, j'ai mis (req,res) =>

homeRouter.get('/home/se-connecter',_homeController.seConnecterGET)
homeRouter.post('/home/se-connecter',_homeController.seConnecterPOST)


homeRouter.get('/home/creer-compte',_homeController.creerCompteGET)
homeRouter.post('/home/creer-compte',_homeController.creerComptePOST)

homeRouter.post('/admin/upload',upload.single('maPhoto'),_adminController.uploadImage)

module.exports = homeRouter