console.log(" ******************************* app lancé ******************************* ")

const express = require('express');
const app = express();
const _homeRouter = require('./routes/home-router')

////////////
// get the client
const mysql = require('mysql2');

// create the connection to database
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'paysage-site'
});

// simple query
connection.query(
  'SELECT * FROM `utilisateur`',
  function(err, results, fields) {
    //console.log(results); // results contains rows returned by server
    //console.log(fields); // fields contains extra meta data about results, if available
  }
);
////////////

app.set('view engine', 'ejs');
app.set('views', './views')

app.use(express.urlencoded({ extended: true }));

app.use(express.static('public'))
app.use(express.static('storage'));
 
app.use(_homeRouter)

 

app.listen(8080);