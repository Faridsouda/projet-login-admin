const imageModel = require('../models/image-model')

const adminController:Object = {

    uploadImage: (req:Request,res:Response) => {
        
        let j = retourneObjet(req)
        //console.log(j)
        imageModel.insert(req.body.titre,j.nomFichier)
        //listePhotos.push(j)
        res.render('admin/confirm-upload')
        
        return;
    }
}

function retourneObjet(req) {
    let infosPhoto = {
        nomFichier: "",
        description:"",
        originalname:"",
        mimetype:""
    }
    infosPhoto.nomFichier = req.file.filename;
    infosPhoto.originalname = req.file.originalname
    infosPhoto.mimetype = req.file.mimetype
    return infosPhoto
}

module.exports = adminController