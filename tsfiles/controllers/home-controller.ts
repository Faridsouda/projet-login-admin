
const _utilisateurModel = require('../models/utilisateur-model')

const homeController:object = {

    index: (req:Request,res:Response) =>{
        
        console.log('homeController.index')
        
        
        res.render('home/index')
    },

    seConnecterGET: (req:Request,res:Response) => {
        console.log("homeController.seConnecter");

        _utilisateurModel.lesImages()
        .then(result => {
            console.log(result)
            res.render('home/login')
        })
        .catch(err => {
            console.log(err)
        })
        
        
    },

    seConnecterPOST: (req:Request,res:Response) => {
        //console.log("homeController.seConnecterPOST");
        let lemail:String
        _utilisateurModel.login(req.body.email,req.body.pwd)
        .then(result => {
            lemail = result.email
            if (result.email === "faridsouda@yahoo.fr"){
                res.render("admin/compte-admin",{email:result.email})
            }
            else{
                //res.render("home/compte",{email:result.email})
                _utilisateurModel.lesImages()
                .then(result => {
                    console.log(result)
                    res.render('home/compte',{lesImages : result,email:lemail })
                })
                .catch(err => {
                    console.log(err)
                })
            }
        })
        .catch(err => {
            res.render("home/index")
        })
        
        
        
        
        
    },

    creerCompteGET: (req:Request,res:Response) => {
        console.log("homeController.creerCompteGET");
        res.render("home/creer-compte")
    },

    creerComptePOST: (req:Request,res:Response) => {
        console.log("homeController.creerComptePOST");
        _utilisateurModel.insert(req.body.email,req.body.pseudo,req.body.pwd)
        res.render("home/compte",{pseudo : req.body.pseudo})
    },

    /*
    afficherImages: (req:Request,res:Response) => {
        
        
    }
    */
}

module.exports = homeController

